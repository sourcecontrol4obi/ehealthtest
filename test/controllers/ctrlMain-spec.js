describe('ctrlMain spec', function(){
    var $rootScope, $scope, ctrlMain;
    
    var unique = 'test/' + (new Date().getTime());
    var note = {
        _id : unique,
        title : 'firt test',
        body : 'Hello World'
    };
    
    beforeEach(function(){
        module('app');
        
        inject(function($injector){
           $rootScope = $injector.get('$rootScope'); 
           $scope = $rootScope.$new();
           ctrlMain = $injector.get('$controller')('ctrlMain', {$scope : $scope});
        });
    });
   
    describe('add', function(){
        it('should add a note', function(){
            $scope.add(note.title, note.body).then(function(){
                expect($scope.notes).to.be.an('array');
                expect($scope.notes).to.have.length.above(0);
                console.log($scope.notes);
            });
        })
    })
    
});