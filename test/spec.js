describe('app spec', function(){
    
    beforeEach(function(){
        module('app');
        
        
    })
    
    describe('version', function(){
        it('should be equal to 0.0.1', inject(function(version){
            expect(version).toBe('0.0.1');
        }));
    });
});