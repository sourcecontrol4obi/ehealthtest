describe('svcDB spec', function(){
    var unique = 'test/' + (new Date().getTime());
    var note = {
        _id : unique,
        title : 'firt test',
        body : 'Hello World'
    };
    var svcDB;
    
    
    beforeEach(function(){
        module('app');     
        
        inject(function($injector){
           svcDB = $injector.get('svcDB'); 
        });
    })
    
    describe('info', function(){
        it('should return object with db_name = ehealth', function(){
            svcDB.info().then(function(info){
                expect(info).to.be.an('object');
                expect(info).to.have.property('db_name', 'ehealth');
            });            
        });
    });
    
    describe('put', function(){
        it('should return an object with ok = true', function(){                                    
            svcDB.put(note).then(function(res){
                expect(res).to.be.an('object');
                expect(res).to.have.property('ok', true);
                expect(res).to.have.property('id', unique);
            });
        });
    })
    
    describe('get', function(){
        it('should return a saved note', function(){
            svcDB.get(unique).then(function(note){
                expect(note).to.be.an('object');
                expect(note).to.have.property('title', note.title);
                expect(note).to.have.property('body', note.body);
            })
        })
    })
    
    describe('update', function(){
        it('should update a note', function(){
            svcDB.get(unique).then(function(note){
                var title = 'Changed Title';
                note.title = title;
                svcDB.put(note).then(function(res){                    
                    svcDB.get(unique).then(function(note){
                        expect(note).to.be.an('object');
                        expect(note).to.have.property('title', title);
                        expect(note).to.have.property('body', note.body);
                    });
                })
            })
        })
    })
    
    describe('list', function(){
        it('should return all the saved notes', function(){
            svcDB.list().then(function(res){
                expect(res).to.be.an('object');
                expect(res).to.have.property('total_rows');
            })
        })
    })
});