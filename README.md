#Module Installation

1. Run npm install
2. Run bower install

#Functions Test
To run the test for the functions Questions 2 - 4

run => npm test

#Angular app test with karma
To run the test for the functions Question 1

run => karma start karma.conf.js

#Viewing the angular app
In order to view the angular app

1. run => npm start
2. visit localhost:3000 on browser
 
