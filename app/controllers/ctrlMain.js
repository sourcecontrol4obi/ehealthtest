(function(){
    var app = angular.module('app');
    
    app.controller('ctrlMain', function($scope, svcDB){
        $scope.notes = [];
        
        reload();
        
        function reload(){
            svcDB.list().then(function(res){
                $scope.notes = res;
            });  
        };
        
        $scope.addNew = function(){
            $scope.newNote = {
                title : 'New Title',
                body : 'Note Content Here'  
            };
            $scope.selection = null;
            $scope.addingNew = true;
            $scope.editing = false;
            $scope.viewing = false;
        }
        
        $scope.add = function(title, body){
            var note = {
                _id : 'notes/' + (new Date().getTime()),
                title : title,
                body : body
            }
            var p = svcDB.put(note);
            p.then(function(res){
                note._rev = res.rev;
                $scope.notes.unshift(note); 
                $scope.addingNew = false;
            });
            return p;
        }
        
        $scope.edit = function(note){
            $scope.selection = note;
            $scope.addingNew = false;
            $scope.editing = true;
            $scope.viewing = false;
        }
        
        $scope.view = function(note){
            $scope.selection = note;
            $scope.addingNew = false;
            $scope.editing = false;
            $scope.viewing = true;
        }
        
        $scope.save = function(){
            var p = svcDB.put($scope.selection);
            p.then(function(res){
                $scope.selection._rev = res.rev;
                $scope.editing = false;
                $scope.viewing = true;
            });
            return p;
        }
        
        $scope.cancel = function(){
            $scope.viewing = true;
            $scope.editing = false;
        }
    });
    
}());