(function(){
    var app = angular.module('app');
    
    app.service('svcDB', function(pouchDB, $q){
        var db = pouchDB('ehealth');
        
        return {
           
            info : function(){
                return db.info();
            },
            
            put : function(note){
                return db.put(angular.merge({}, note));
            },
            
            get : function(id){
                return db.get(id);
            },
            
            list : function(){
                var defered = $q.defer();
                db.allDocs({
                    include_docs: true,
                    descending: true
                }).then(function(res){
                    defered.resolve(res.rows.map(function(row){                        
                        return row.doc;
                    }))
                });
                return defered.promise;
            },
            
            
        }
    })
}())