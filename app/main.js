var app = angular.module('app',['pouchdb']);

app.config(function($interpolateProvider) {
    $interpolateProvider.startSymbol('{[{');
    $interpolateProvider.endSymbol('}]}');
});

app.value('version', '0.0.1');