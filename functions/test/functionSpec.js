var fns = require('../fns');
var should = require('should'); 

describe('find_chars(string1, string2)', function(){
    var string1 = "Hello Mr Kay";
    var string2 = "Hello Madam Jay";
    
    it('order N should take 2 strings and return a string that contains only the characters found in string1 and string2'+
        ' should be in the order that they are found in string1', function(){
        fns.find_chars_n1(string1, string2).should.equal("Hello M ay");
    });    
    
    it('order N*N should take 2 strings and return a string that contains only the characters found in string1 and string2'+
        ' should be in the order that they are found in string1', function(){
        fns.find_chars_n2(string1, string2).should.equal("Hello M ay");
    });  
});

describe('ARRAY COMPACTION', function(){
    var array = [1, 3, 7, 7, 8, 9, 9, 9, 10];
    var array2 = [1, 3, 7, 8, 9, 7, 9, 9, 10];
    var result = [1, 3, 7, 8, 9, 10];
    it('should fail if array is not sorted', function(){
        fns.compact(array2).should.not.eql(result);
    })
    it('should accept a sorted array and returns an array without duplicates', function(){
        fns.compact(array).should.eql(result);
    })
});

describe('ROTATING AN ARRAY', function(){
    var array = [1, 2, 3, 4, 5, 6];
    var result = [5, 6, 1, 2, 3, 4];
    var result2 = [2, 3, 4, 5, 6, 1];
    var result3 = [1, 2, 3, 4, 5, 6];    
    it('should accept an array of integers and returns that array rotated by N positions', function(){
        fns.rotate(array, 2).should.eql(result);
        fns.rotate(array, 5).should.eql(result2);
        fns.rotate(array, 6).should.eql(result3);
        fns.rotate(array, 8).should.eql(result);
    })
})

describe('LEAST COMMON MULTIPLE', function(){
    var array = [5, 6, 15, 8, 12, 27];
    var result = 1080;
    it('should take an array of integers and return the LCM', function(){
        fns.LCM(array).should.eql(result);
    })
})