module.exports = fns = {
    
    find_chars_n1 : function(string1, string2){
        var list1 = string1.split("");
        var result = list1.filter(function(i){
            return string2.indexOf(i) > -1
        });        
        return result.join("");
    },
    
    find_chars_n2 : function(string1, string2){
        var result = [];
        
        for(var i in string1){
            var char1 = string1[i];
            for(var j in string2){
                var char2 = string2[j];
                if(char1 == char2){
                    result.push(char1);     
                    break;           
                }
            }
        }
        return result.join("");
    },
    
    compact : function(array){        
        var result = [];
        var item;
        for(var i = 0; i < array.length; i++){
            item = array[i];
            if(result[result.length - 1] != item){
                result.push(item);                
            }
        }
        return result;
    },
    
    rotate : function(array, N){
        var end = array.slice(0, array.length - N);
        var start = array.slice(array.length - N);
        return start.concat(end);
    },
    
    LCM : function(array){        
        var sorter = function(a, b){ 
            return a > b; 
        };       
        //sorted array without duplicates 
        var workingSet = fns.compact(array.sort(sorter));
        var divider, factors = [];        
        while(workingSet.length > 0){            
            divider = workingSet.sort(sorter).shift();
            factors.push(divider);
            workingSet = workingSet.map(function(i){
                return i % divider > 0 ? i : i / divider;
            })            
        }             
        return factors.reduce(function(prev, curr, pos){
           return prev * curr; 
        });
    }
}